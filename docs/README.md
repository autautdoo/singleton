# Singleton

This package contains a meta-class `Singleton`.
You can attach it to any other class the following way:

```python
from singleton import Singleton

# define configuration class as singleton
class Config(metaclass=Singleton):
    def __init__(self):
        self.foo = 'bar'
        
# instantiate configuration
c1 = Config.instance()

# get new configuration instance
c2 = Config.instance()

assert c1 is c2
```

`Singleton` has the following methods:
* `instance` - returns the instance of the given class
* `configure` - if your class requires some additional parameters, you can define them first using this method.
    Next time you call `instance` method, the instance with configured parameters will be returned
* `instance_exists` - returns True if instance exists, and False if it doesn't
* `uninstantiate` - removes an existing instance. Next call to `instance` method will return completely different instance

## Usage cookbook

Example below shows the exact way you should use `Singleton`

```python
from singleton import Singleton

# class which we will define as Singleton
class Config(metaclass=Singleton):
    def __init__(self, foo):
        self.bar = foo
        
c1 = Config.configure(foo='some_arg_value').instance()
c2 = Config.instance()
assert c1 is c2

# instance exists
assert Config.instance_exists() is True
Config.uninstantiate()

# instance no longer exists
assert Config.instance_exists() is False

Config.configure(foo='another_arg_value')
c3 = Config.instance()

assert Config.instance_exists() is True
assert c3 is not c1
```
