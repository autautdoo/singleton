from setuptools import setup, find_packages

with open('docs/README.md') as readme:
    long_description = readme.read()

setup(
    name='singleton',
    version='1.0.0',
    python_requires='>=3',
    author='Domagoj Šitum',
    author_email='dsitum@igpte.ch',
    description='Singleton implementation with additional features',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://bitbucket.org/autautdoo/singleton',
    packages=find_packages(exclude=['test*']),
    # install_requires=[],
    # package_data={},
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Topic :: Software Development :: Libraries'
    ]
)
