

class Singleton(type):
    """
    Metaclass which helps defining other classes as singletons.
    You can find more info about singleton design pattern here:
    https://en.wikipedia.org/wiki/Singleton_pattern

    Other than usual method which returns the instance, there are also the methods to
    pass arguments to underlying class, check if instance exists without instantiating it,
    to remove instance, etc.

    Note:
        `Singleton` is not meant to be instantiated directly. It's intended to be used as a metaclass.

    Examples:
        from singleton import Singleton

        # class which we will define as Singleton
        class Config(metaclass=Singleton):
            def __init__(self, foo):
                self.bar = foo

        c1 = Config.configure(foo='some_arg_value').instance()
        c2 = Config.instance()
        assert c1 is c2

        # instance exists
        assert Config.instance_exists() is True
        Config.uninstantiate()

        # instance no longer exists
        assert Config.instance_exists() is False

        Config.configure(foo='another_arg_value')
        c3 = Config.instance()

        assert Config.instance_exists() is True
        assert c3 is not c1
    """
    _instance = None

    def __call__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instance

    def configure(self, *args, **kwargs):
        """
        Can be used to send some parameters upon instantiating.
        Every positional or keyword arguments sent to this method will be passed to the
        constructor of underlying class.

        Notes:
            Every new call to this method will create new instance, instead of returning the same one.
            Ensure that you clean-up all the resources that existing instance might use before calling it.

        Args:
            *args: Variable length argument list.
            **kwargs: Arbitrary keyword arguments.

        Returns:
            Singleton: The Singleton class itself, making the procedure of configuring and instantiating chainable
        """
        self._instance = None
        self(*args, **kwargs)
        return self

    def instance(self):
        """
        Creates new instance of the underlaying object or returns existing one, if this method or ``configure``
        has been used before.

        Returns:
            Instance of the underlying class.
        """
        return self()

    def instance_exists(self):
        """
        Checks whether the singleton instance exists.

        Returns:
            bool: ``True`` if instance exists. Otherwise, returns ``False``
        """
        return self._instance is not None

    def uninstantiate(self):
        """
        Simply removes reference to the existing istance.
        The developer should take care of closing the resources, which the underlying class used, if there are any.
        """
        self._instance = None
